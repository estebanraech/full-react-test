import React, { Component } from 'react';

class Header extends Component {
  render() {
    return (
      <div className="banner header">
    	<Logo link={require('./assets/heroguestlogo.png')} />
    	<SocialMedia link={require('./assets/testImg2.png')} image={require('./assets/insta.png')} />
    	<SocialMedia link={require('./assets/testImg3.png')} image={require('./assets/fb.png')} />
    	<SocialMedia link={require('./assets/testImg4.png')} image={require('./assets/twitter.png')} />
      </div>
    );
  }
}

class Logo extends Component{
	constructor(props){
		super(props);
		this.link = props.link;
	}
	render(){
		
		return(
			<div className="logo">
				<img src={this.link} />
			</div>
		);
	}
}

class SocialMedia extends Component{
	constructor(props){
		super(props);
		this.link = props.link;
		this.image = props.image;
	}
	render(){
		return(
			<div className="sm">
				<a href={this.link}>
					<img src={this.image} />
				</a>
			</div>
		);
	}
}
export default Header;
