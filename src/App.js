import React, { Component } from 'react';
import Header from './Header';
import SideMenu from './SideMenu';
import Content from './Content';

class App extends Component {
  render() {
    return ([
      <Header></Header>,
      <SideMenu></SideMenu>,
      <Content></Content>
      ]);
  }
}

export default App;
